# Class Assignment 4

## This project demonstrates how to run a container with Tomcat and other with H2.

### You should produce a solution similar to the one of the part 2 of the previous CA but now using Docker instead of Vagrant.

You should use docker-compose to produce 2 services/containers:
web: this container is used to run Tomcat and the spring application
db: this container is used to execute the H2 server database

On this assignment we had three different possibilities of installing Docker depending on each OS: Docker Desktop, Docker Toolbox and Docker Engine.

I started to install Docker Toolbox because my OS was Windows 10 Home and didn't support hyper-v, however I didn't manage to execute Docker Toolbox either because my system wasn't allowing it, an error was constantly appearing and I went to the system BIOS to check if hyper-v was switched off but I couldn't find any reference to it.

Therefore, my only option left was to either trying to update my Windows to Pro in order to install Docker Desktop or to install Docker Engine for Ubuntu on my VM.

I chose the latter because updating to Windows Pro was too expensive.

So, after some connection problems with networks on my VM I managed to properly connect the VM to the host environment and the Web (some IP problems and configurations that weren't correct).

After those issues were addressed I proceded with the installation of Docker Engine which went smoothly, and the download of the example repository which was available to us from:

`
https://bitbucket.org/atb/docker-compose-spring-tut-demo/
`

After the download into my host environment I used Filezilla to transfer those files to my VM, using the IP of the VM, username and password.

After doing so, I changed directories into the example file using cd command and proceded to the web folder in order to modify the dockerfile located there and to update it, so that it would execute my gradle project, as required. 

To this end, I executed the command 

`
sudo nano Dockerfile
`

which means that the superhost executes a text editor program on file Dockerfile in order to modify it.

Consequently the text editor appeared and I changed the git clone command RUN so that it would clone my repository:

`
RUN git clone https://1191756@bitbucket.org/1191756/devops-19-20-b.git
`

And also the WORKDIR command so that it would change directories to proper folder:

`
WORKDIR /tmp/build/devops-19-20-b/ca2/Part2
`

After this, I only had to change the 'RUN cp' command so that it would copy the right file since my war file wasn't named 'basic-0.0.1-SNAPSHOT.war' but 'demo-0.0.1-SNAPSHOT.war':

`
RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
`

The Dockerfile located on the DB folder or file docker-compose.yml were accurate and already modified to execute my repository so I didn´t need to modify them.

So after this I executed in the folder of the docker-compose.yml:

`docker-compose build`

To buil my project, and command

`docker-compose up`

for it to execute.

However, a permission error came up on my VM, as it did on the previous Class Assignment, so I remembered the commands I used last time in order to get permission, so I executed once again:

`
sudo nano Dockerfile
`

On the project's web folder and added this command to it:

`
RUN chmod u+x gradlew
`

After this, I went back to the example's root directory and executed the commands again, which this time seemed to be properly executing.

To confirm it I had to open the spring web application in my host environment(since there is no GUI in my Ubuntu VM), using the IP of my VM:


http://192.168.233.100:8080/demo-0.0.1-SNAPSHOT/


and checked that the output was correct.

But also checked the H2 console using the following url:


http://192.168.233.100:8080/demo-0.0.1-SNAPSHOT/h2-console


For the connection string I used: 

`
jdbc:h2:tcp://192.168.33.11:9092/./jpadb  
`

And checked that the H2 console was working properly.

### Publish the images (db and web) to Docker Hub (https://hub.docker.com).

First I ran command 'docker images' to check which images I had on my ubuntu virtual machine, which were ubuntu, apache, db and web images related to the current project.

So the next step was to push the images to docker hub because I already signed in on docker hub and created a repository.

Thus I executed command: 

`
sudo docker push atbdockercomposespringtutdemo1d63e00f58fe_web:v1.0
`

however, my request was denied because ubuntu didn't know were to push it, because I didn't login on DockerHub.

So I executed command:

`
sudo docker login
`

and wrote my username and password.

Unfortunatelly, I couldn't login either because of a problem with permissions, so I went to stackoverflow and looked for solutions and came up with one.

I executed the following commands:

`
sudo apt update
sudo apt -V install gnupg2 pass
`

And proceded again with the login, which worked this time.

Then I executed command:

`
sudo docker push atbdockercomposespringtutdemo1d63e00f58fe_db:v1.0
`

in order to push the db image to the repository, since the other one was too big to upload, it's size was 1.45 GB.

### Use a volume with the db container to get a copy of the database file by using the exec to run a shell in the container and copying the database file to the volume.

First I ran command 

`
sudo docker-compose -d
`

to run both containers on background, and then I executed the command 

`
sudo docker-compose ps
`

to check if the containers were already running.

After this I had to change my working directory to the root directory of the database container, so I executed command 
`
sudo docker-compose exec db /bin/bash 
`
in order to open a bash terminal on that said directory.

Then I executed:

`cp jpadb.mv.db /usr/src/data`

to copy the database file to the volume.

Finally I checked if the file was in the proper directory with 'ls' command, which was correct.

### Tag

In the end of the assignment, a new tag was added: `git tag ca4`

And pushed to the repository:  `git push origin ca4`