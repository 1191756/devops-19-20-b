Class Assignment 2 Part 1 Report
===================

This is a demo application that implements a basic multithreaded chat room server.

The server supports several simultaneous clients through multithreading. When a client connects the server requests a 
screen name, and keeps requesting a name until a unique one is received. After a client submits a unique name, the 
server acknowledges it. Then all messages from that client will be broadcast to all other clients that have submitted a 
unique screen name. A simple "chat protocol" is used for managing a user's registration/leaving and message broadcast.


Requirements
-------------

1. You should start by downloading and commit to your repository (in a folder for Part1 of CA2) the example application 
available at https://bitbucket.org/luisnogueira/gradle_basic_demo/. You will be working with this example.

2. Read the instructions available in the readme.md file and experiment with the application.

3. Add a new task to execute the server.

4. Add a simple unit test and update the gradle script so that it is able to execute the test. 
The unit tests require junit 4.12 to execute. Do not forget to add this dependency in gradle.

5. Add a new task of type Copy to be used to make a backup of the sources of the application.
It should copy the contents of the src folder to a new backup folder.

6. Add a new task of type Zip to be used to make an archive (i.e., zip file) of the sources of the application.
It should copy the contents of the src folder to a new zip file.

7. At the end of the part 1 of this assignment mark your repository with the tag ca2-part1.


Analysis, Design and Implementation of the requirements
-------------------------------------------------------

Remebering that my local repository was already synchronized with my remote repository, all I had to do was to upload those
files to the remote, whitch I should have done using the following commands:

```
git add --all
git commit -m "Added project files. Created folder for Part 1 of CA2 and its readme file"
git push
```

Unfortunatelly it seems that bitbucket didn't save my commit for some reason, probably because of some steps that I didn't
do corretly on the first assignment, but after I realized it I've started a new local repository and synchronized it so the
situation has been addressed and everything has been working promptly ever since.

## Build

The next step was to **Read the instructions available in the readme.md file and experiment with the application.**.

~~~
To build a .jar file with the application:

    % ./gradlew build 
~~~

However, it seemed that both my path and java_home environment variables were not correctly installed, as well as the 
java version that I had been working on, so I had to uninstall the version I had, download java jdk 1.8 and to set the 
above mentioned environment variables straight, which I did. Only than I was able to generate a .jar file for the 
assignment, using gradle.

While Gradle executed this command, several messages have been shown on the terminal, listing all the tasks being completed 
and a success message appeared.

This command added a build folder to the project with all the assignment's files.

The recommended way to execute any Gradle build is with the help of the Gradle Wrapper. The Wrapper is a script that invokes
a declared version of Gradle, downloading it beforehand if necessary. As a result, developers can get up and running with a
Gradle project quickly without having to follow manual installation processes. 

## Run the server

To run the server, it was executed:
```
java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 63342
``` 

with the following output:

```
The chat server is running...
```

Next, the ChatClientApp has been run in other terminal:

```
./gradlew runClient
```

A success build message appeared and a chat window initialized. 


## Add a new task to execute the server.

A new task needed to be created to execute the server.

So, in the build.gradle file it was added a task similar to the runClient task but referring to the **ChatServerApp class**:

```
task executeServer(type: JavaExec, dependsOn: classes) {
    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatServerApp'

    args '59001'
}
```

To check if this code was correct, there were two ways:

**a)** Listing the available tasks with:

```
./gradlew tasks --all
```

**b)** Running the server, by executing the task:

```
./gradlew executeServer
```

This command had the same output as the one previously executed to run the server.


## Add a unit test and update the Gradle script.

I've started to create a test class in this path **src/test/java/basic_demo/AppTest**.

In this test class, I added the code given in the instructions.

Afterwards, given that the unit tests require junit 4.12 to execute, I had to update the gradle script dependencies so that 
gradle could execute the test:

```
dependencies {
    // Use Apache Log4J for logging
    compile group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
    compile group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
    testCompile group: 'junit', name: 'junit', version: '4.12'
}
```

After running the unit test with success, I created an issue and commited these changes to the repository.

    % git add --all
      git commit -m "fix #10; Add unit test and update gradle script."


## Add a new task of type Copy to be used to make a backup of the sources of the application.


In order to create a backup of the sources of the application I added a new task to `build.gradle`:

```
task backupSrcIntoNewFolder(type:Copy){
    from 'src'
    into 'backupSrc'
}
```

The task copies files in `src` to a new folder named `backupSrc`.

Then, I created an issue and commited these changes to the repository.

    % git add --all
      git commit -m "fix #12; New task to backup the sources of the application."


## Add a new task of type Zip to be used to make an archive (i.e., zip file) of the sources of the application.


In order to create an archive of the sources of the application I added a new task to `build.gradle`:

~~~
task zipFileOfSrc (type:Zip) {
    from 'src'
    archiveFileName = 'src.zip'
    destinationDirectory = file('zip')
}
~~~

This task copies the contents of the src folder to a new zip folder named `zip`.

Then, I created an issue and commited these changes to the repository.

~~~
git add --all
git commit -m "Fix #13; New task of type Zip to be used to make an archive of the sources of the application."
~~~

To conclude, I tagged the repository in order to flag the first part of the second assignment: 

```
git tag ca2-part1
git push origin ca2-part1
```
