#README

##Class Assignment 3 (Part1) - Virtualization with Vagrant

The goal of Part 1 of this assignment is to practice with VirtualBox using the same
projects from the previous assignments but now inside a VirtualBox VM with Ubuntu.


###Preparation of a Virtual Machine by using VirtualBox as hypervisor.

1. Started by creating a Virtual Machine with Linux as OS;
2. Download of image (ISO file) to install Ubuntu 18.4, using minimalCD;
3. In the Host Network Manager of VirtualBox, defined a Host-only Adapter with the IP 192.168.56.1/24;
4. Installation of network tools:

```bash
sudo apt install net-tools
```
5.Edition of the network configuration file to setup the IP:
```bash
sudo nano /etc/netplan/01-netcfg.yaml
```

...
## Class Assignment 
After creating the Virtual Machine

- Git Clone of my repository to Ubuntu:  
`git clone https://1191756@bitbucket.org/1191756/devops-19-20-b.git`

- Installation of openssh-server so that we can use ssh to open secure terminal sessions to the VM (from other hosts):
```
sudo apt install openssh-server
```
- Enabled password authentication for ssh
```
sudo nano /etc/ssh/sshd_config
uncomment the line PasswordAuthentication yes
sudo service ssh restart
```

- Installation of a ftp server so that we can use the FTP protocol to transfers files to/from
the VM (from other hosts):
```
sudo apt install vsftpd
```

- Enable of write access for vsftpd:
```
sudo nano /etc/vsftpd.conf
uncomment the line write_enable=YES
sudo service vsftpd restart
```

Use of ssh to connect to the VM, accessing in host CLI:
```
ssh diogoamr@192.168.56.1
```

Since the FTP server is enabled in the VM we can now use an ftp application, like FileZilla, to transfer files to/from the VM.

- Proceded with the installation of git, jdk 8, maven and gradle on the VM and edition of the
environment variables:
```
sudo apt install git
sudo apt install openjdk-8-jdk-headless
sudo apt install maven
sudo apt install gradle
```

- Made a clone of the application in the VM:
```
git clone https://github.com/spring-guides/tut-react-and-spring-data-rest.git
```

- Execution of the application in basic directory:
```
./mvnw spring-boot:run
```

## Building and Executing projects from previous assignments
### Class assignment 1 - Spring Boot Tutorial Basic Project

- Went into the project directory
```
cd /devops-19-20-b/ca1
```
- Execution of the following command to run the spring-boot application:
```
mvn gwt:run
```
- Given that all features and requisites were already installed in a previous assignment, I 
could access on Google Chrome on the following URL: 'http://192.168.56.5:8080/' that the server was properly executing.

### Class assignment 2 - Gradle_Basic_Demo Project

- Regarding the execution of Ca2 Part1, I executed the following command so that gradle could build the refered project:

```
gradle build
```

- Next, regarding the execution of the chat server, this command was executed:

```
gradle runServer
```

- Keeping in mind that Ubunt wasn't installed with a Graphical User Interface, it wasn't possible to execute task runClient, necessary to launch the chat, so it was necessary to execute that command in the host CLI, which I did:

```
gradle task runClient
```
- However the chat window wasn't launching correctly so in order to make the client work, I changed the IP of the task in build.gradle from "localhost" to the IP of my VM:

'192.168.56.100'. 

Executed the command once more and the expected chat window was successfully launched.

### Tag

In the end of the assignment, a new tag was added to the repository: `git tag ca3-part1`

And pushed to the repository:  `git push origin ca3-part1`


