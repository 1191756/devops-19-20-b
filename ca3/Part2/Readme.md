# Class Assignment 3 - Part 2

The goal of Part 2 of this assignment is to use Vagrant to setup a virtual environment
to execute the tutorial spring boot application, gradle "basic" version (developed in
CA2, Part2).

1. I started by opening the URL https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/, that has a vagrantFile and read it carefully to try to understand the different configurations that were conceived.

2. Copied VagrantFile to a local repository in the host OS.

3. Experimented with the application, through the execution of command:

````
vagrant up
````

4. Update of VagrantFile configuration to my gradle version of the spring application:

````
# Change the following command to clone your own repository!
      git clone https://1191756@bitbucket.org/1191756/devops-19-20-b.git
      cd devops-19-20-b/ca2/Part2
````

- Insertion of the correct git clone URL and the project's location inside it.

- Rename of the war file located in build/libs, from "basic..." to "demo..." because that is how it was named in my project:

````
# To deploy the war file to tomcat8 do the following command:
      sudo cp build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
````

Check https://bitbucket.org/atb/tut-basic-gradle to see the changes
necessary so that the spring application uses the H2 server in the db VM. Replicate
the changes in your own version of the spring application.

- Support for building war file;

- Support for h2 console;

- Weballowothers to h2;

- Application context path;

- Fixes ref to css in index.html;

- Settings for remote h2 database;

- Fixes h2 connection string;

- Update so that spring will no longer drop the database on every execution;

# Problems encountered

Unfortunatelly I ran into some problems regarding permissions between the web VM and my repository in Bitbucket.

I ran different configurations and I ultimatelly had success by changing the location in VagrantFile of the folder that was to be provided in the remote repository and by renaming the war file, in two javascript files and the war file inside build.gradle (as mentioned above). Afterwards I changed the URL to get access to the list of employees.   

### Tag

In the end of the assignment, a new tag was added: `git tag ca3-part2`

And pushed to the repository:  `git push origin ca3-part2`

