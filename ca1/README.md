# Class Assignment 1 Report


## 1. Analysis, Design and Implementation

## Fase Inicial

Comecei o projeto por aceder à pasta de Devops, sendo esse o meu local path, em C:\Users\diogo\Documents\DevOps - Projetos\ca1.

Dentro da pasta indicada iniciei o GitBash e inseri o comando "git init" para indicar ao sistema que iria iniciar um repositório git nesse local.

Conetei o meu local repository ao remote repository através do comando "git remote add origin https://1191756@bitbucket.org/1191756/devops-19-20-b.git".

De seguida entrei no Bitbucket e iniciei um novo repositório.

Fiz um clone do projeto base, utilizando GitBash na pasta que indiquei, com o comando "git clone git@bitbucket.org:atb/devops-19-20-b-12312234.git".

De seguida adicionei todos os ficheiros que clonei para a minha pasta pessoal, através do comando "git add.".

Corri o comando "git status" para verificar que os ficheiros foram adicionados(ou "staged") e conclui que sim.

Corri o comando "git diff" para verificar o que foi preparado para commit e o que não foi.

Fiz então o commit inicial dos ficheiros através do comando git commit -m "initial commit"' e de seguida "git push".

## Use the basic version of the app (i.e., basic folder) and try to add a new jobTitle field (String data type) to Employee. Commit your new code to the repository.

Abri o projeto no IDE Intellij, adicionei o campo jobTitle (adicionei no início race pois o tema do repositório é um filme, na altura pareceu 
fazer mais sentido mas mais tarde fiz refactor para JobTitle) na classe Employee e fiz as alterações necessárias nas outras classes de modo a 
que estas suportem o novo parâmetro.

Utilizei o comando "git add .", para preparar as alterações, git commit -m "New jobTitle field (String data type) to Employee"' e "git push".

Também fiz a tag desta versão, através do comando "git tag -a v1.2.0 -m "my version 1.2.0".

Seguidamente, fiz "git push origin v1.2.0" .

## You should implement a simple scenario illustrating a git workflow.

Comecei por criar um novo ramo, comando "git branch email-field".

Mudei o master(indica o ramo que estou a desenvolver) para o novo ramo, comando "git checkout email-field".

Seguidamente, inseri o novo parâmetro email na classe Employee e alterei todas as classes necessárias para suportar tal adição.

Inseri testes unitários para validar se os parêmetros da classe Employee são válidos (validações se vazios ou nulos).
 
Fiz commit e push através do IDE.

De seguida, fiz o comando "git checkout master" para voltar para o branch 'master'.

Só aí é que pude fazer merge entre o master e o ramo email-field, comando "git merge email-field".

Também fiz a tag desta versão, através do comando "git tag -a v1.3.0 -m "version 1.3.0".

Seguidamente, fiz "git push origin v1.3.0".

## You should also create branches for fixing bugs (e.g., "fix-invalid-email").

Comecei por criar um novo ramo, comando "git branch fix-invalid-email".

Mudei o master(indica o ramo que estou a desenvolver) para o novo ramo, comando "git checkout fix-invalid-email".

Inseri testes unitários para validar se o parêmetro/objeto email da classe Employee é válido (acrescentei validações 
de modo a verificar se usa @ e respeita o tipo de carateres universais, sem adições de carateres especiais pois apesar
de não ser pedido achei que também fazia sentido).

Fiz commit e push através do Intellij;

De seguida, fiz o comando "git checkout master" para voltar para o branch 'master';

Só aí é que pude fazer merge entre o master e o ramo fix-invalid-email, comando "git merge fix-invalid-email";

Também fiz a tag desta versão, através do comando "git tag -a v1.3.1 -m "version 1.3.1";

Seguidamente, fiz "git push origin v1.3.1"; 


## 2. Analysis of an Alternative


Existem várias alternativas a tecnologias de version control, no entanto eu escolhi Mercurial pela sua versatilidade e facilidade
de uso ("user friendly").

Tanto Mercurial como Git são DVCS, o que permite aos developers copiar, manter e atualizar repositórios de código e de trabalho
em equipa, desenvolvendo engenharia de software em conjunto.

No geral, a opinião geral é que Git é mais complexo e que pode causar alguns problemas entre developers menos experientes no entanto, 
Git é mais indicado para developers mais experientes pelas suas caraterísticas em especial, no que respeita à manipulação de ramos
de desenvolvimento de código.

No entanto, Git é conhecido universalmente por ser muito efetivo no que toca a desenvolver código em diferentes ramos, pois tem 
uma staging area que permite decidir que ficheiros devem ser submetidos para o repositório remoto, além disso, permite criar, 
remover e alterar ramos de desenvolvimento a qualquer altura.

Em Mercurial não existe staging area pelo que as alterações são submetidas diretamente no repositório, só utilizando uma extensão 
é possível escolher os ficheiros que se quer submeter, além disso os ramos em Mercurial funcionam de um modo diferente
pois referem-se a um conjunto de alterações referentes a um ficheiro no repositório, sendo guardadas indefinidamente, pelo que
os ramos não podem ser removidos.

Git é assim a escolha óbvia, motivo pelo qual é líder de mercado pois oferece ferramentas mais profissionais e completas relativamente 
a outras tecnologias de version control, sendo indicado para quem desenvolve engenharia de software, enquanto que Mercurial é mais 
indicado para quem desenvolve software pouco técnico ou de relativa complexidade. 


## 3. Implementation of the Alternative


Como não tive tempo para implementar todos os comandos optei por configurar o Mercurial com o repositório remoto no Helix TeamHub, 
pelo que de seguida irei demonstrar como o fiz.

Comecei por instalar o Mercurial versão 5.0.2 pois a versão 5.3 não funcionou no meu IDE (Intellij), sendo que seguidamente iniciei uma
nova conta conta e um novo repositório remoto no Helix TeamHub, tendo enviado convite a dar acesso como user para atb@isep.ipp.pt, como 
também fiz no Bitbucket em Git.

Iniciei uma nova pasta no meu repositório local chamada ca1_alt, tendo copiado o meu repositório local que desenvolvi em Git para essa 
pasta.

Seguidamente, iniciei um novo repositório de Mercurial nessa pasta, tendo para o efeito selecionado um username, criando uma pasta hg e um
ficheiro hg_ignore.

Entrei no Intellij e importei o projeto selecionando o ficheiro pom.xml.

Com agrado, pude verificar que o repositório já se encontrava sincronizado com o repositório remoto do Helix TeamHub, pelo que
me restava implementar os respetivos comandos de Mercurial:

"hg init" para dar ordem de iniciar um novo projeto;

"hg add ." para dar ordem de tracking aos ficheiros em mercurial;

"hg commit -m 'Initial Commit' que prepara os ficheiros para o repositório juntamente com a referida mensagem;

"hg push" para enviar os ficheiros para o repositório.
