package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @DisplayName("getFirstNameEmpty - Empty FirstName")
    @Test
    void getFirstNameEmpty() {
        //Arrange
        Employee e1 = new Employee("","Silva", "Técnico", "Humana", "jose@gmail.com" );
        //Assert
        assertThrows(IllegalArgumentException.class, () -> e1.getFirstName());
    }

    @DisplayName("getFirstNameNull - Null FirstName")
    @Test
    void getFirstNameNull() {
        //Arrange
        Employee e1 = new Employee(null,"Silva", "Técnico", "Humana", "jose@gmail.com" );
        //Assert
        assertThrows(NullPointerException.class, () -> e1.getFirstName());
    }

    @DisplayName("setFirstNameEmpty - Empty FirstName")
    @Test
    void setFirstNameEmpty() {
        //Arrange
        Employee e1 = new Employee("","Silva", "Técnico", "Humana", "jose@gmail.com" );
        //Assert
        assertThrows(IllegalArgumentException.class, () -> e1.setFirstName(e1.getFirstName()));
    }

    @DisplayName("setFirstNameNull - Null FirstName")
    @Test
    void setFirstNameNull() {
        //Arrange
        Employee e1 = new Employee(null,"Silva", "Técnico", "Humana", "jose@gmail.com" );
        //Assert
        assertThrows(NullPointerException.class, () -> e1.setFirstName(e1.getFirstName()));
    }

    @DisplayName("getLastNameEmpty - Empty LastName")
    @Test
    void getLastNameEmpty() {
        //Arrange
        Employee e1 = new Employee("José","", "Técnico", "Humana", "jose@gmail.com" );
        //Assert
        assertThrows(IllegalArgumentException.class, () -> e1.getLastName());
    }

    @DisplayName("getLastNameNull - Null LastName")
    @Test
    void getLastNameNull() {
        //Arrange
        Employee e1 = new Employee("Jose",null, "Técnico", "Humana", "jose@gmail.com" );
        //Assert
        assertThrows(NullPointerException.class, () -> e1.getLastName());
    }

    @DisplayName("setLastNameEmpty - Empty LastName")
    @Test
    void setLastNameEmpty() {
        //Arrange
        Employee e1 = new Employee("José","", "Técnico", "Humana", "jose@gmail.com" );
        //Assert
        assertThrows(IllegalArgumentException.class, () -> e1.setLastName(e1.getLastName()));
    }

    @DisplayName("setLastNameNull - Null LastName")
    @Test
    void setLastNameNull() {
        //Arrange
        Employee e1 = new Employee("Jose",null, "Técnico", "Humana", "jose@gmail.com" );
        //Assert
        assertThrows(NullPointerException.class, () -> e1.setLastName(e1.getLastName()));
    }

    @DisplayName("getDescriptionNull - Empty Description")
    @Test
    void getDescriptionEmpty() {
        //Arrange
        Employee e1 = new Employee("José","Silva", "", "Humana", "jose@gmail.com" );
        //Assert
        assertThrows(IllegalArgumentException.class, () -> e1.getDescription());
    }

    @DisplayName("getDescriptionNull - Null Description")
    @Test
    void getDescriptionNull() {
        //Arrange
        Employee e1 = new Employee("Jose","Silva", null, "Humana", "jose@gmail.com" );
        //Assert
        assertThrows(NullPointerException.class, () -> e1.getDescription());
    }

    @DisplayName("setDescriptionEmpty - Empty Description")
    @Test
    void setDescriptionEmpty() {
        //Arrange
        Employee e1 = new Employee("José","Silva", "", "Humana", "jose@gmail.com" );
        //Assert
        assertThrows(IllegalArgumentException.class, () -> e1.setDescription(e1.getDescription()));
    }

    @DisplayName("setDescriptionNull - Null Description")
    @Test
    void setDescriptionNull() {
        //Arrange
        Employee e1 = new Employee("Jose","Silva", null, "Humana", "jose@gmail.com" );
        //Assert
        assertThrows(NullPointerException.class, () -> e1.setDescription(e1.getDescription()));
    }

    @DisplayName("getRaceNull - Empty Race")
    @Test
    void getRaceEmpty() {
        //Arrange
        Employee e1 = new Employee("José","Silva", "Técnico", "", "jose@gmail.com" );
        //Assert
        assertThrows(IllegalArgumentException.class, () -> e1.getJobTitle());
    }

    @DisplayName("getRaceNull - Null Race")
    @Test
    void getRaceNull() {
        //Arrange
        Employee e1 = new Employee("Jose","Silva", "Técnico", null, "jose@gmail.com" );
        //Assert
        assertThrows(NullPointerException.class, () -> e1.getJobTitle());
    }

    @DisplayName("Set race - Empty Race")
    @Test
    void setRaceEmpty() {
        //Arrange
        Employee e1 = new Employee("José","Silva", "Técnico", "", "jose@gmail.com" );
        //Assert
        assertThrows(IllegalArgumentException.class, () -> e1.setJobTitle(e1.getJobTitle()));
    }

    @DisplayName("Set race - Null Race")
    @Test
    void setRaceNull() {
        //Arrange
        Employee e1 = new Employee("Jose","Silva", "Técnico", null, "jose@gmail.com" );
        //Assert
        assertThrows(NullPointerException.class, () -> e1.setJobTitle(e1.getJobTitle()));
    }

    @DisplayName("Create email - Empty Email")
    @Test
    void getEmailEmpty() {
        //Arrange
        Employee e1 = new Employee("José","Silva", "Técnico", "Humana", "" );
        //Assert
        assertThrows(IllegalArgumentException.class, () -> e1.getEmail());
    }

    @DisplayName("Create email - Null Email")
    @Test
    void getEmailNull() {
        //Arrange
        Employee e1 = new Employee("Jose","Silva", "Técnico", "Humana", null );
        //Assert
        assertThrows(NullPointerException.class, () -> e1.getEmail());
    }

    @DisplayName("Create email - Happy Path")
    @Test
    void getEmailIfValidHappyPath() {
        //Arrange
        String email = "email-teste@gmail.com";
        Employee e1 = new Employee("Jose","Silva", "Técnico", "Humana", email );
        //Act
        String expected = "email-teste@gmail.com";
        String result = e1.getEmail();
        //Assert
        assertEquals(result,expected);
    }

    @DisplayName("Set email - Empty Email")
    @Test
    void setEmailEmpty() {
        //Arrange
        Employee e1 = new Employee("José","Silva", "Técnico", "Humana", "" );
        //Assert
        assertThrows(IllegalArgumentException.class, () -> e1.setEmail(e1.getEmail()));
    }

    @DisplayName("Set email - Null Email")
    @Test
    void setEmailNull() {
        //Arrange
        Employee e1 = new Employee("Jose","Silva", "Técnico", "Humana", null );
        //Assert
        assertThrows(NullPointerException.class, () -> e1.setEmail(e1.getEmail()));
    }
}