# Class Assignment 5 Part 1

### The goal of the Part 1 of this assignment is to practice with Jenkins using the "gradle basic demo" project that should already be present in the student’s individual repository.

The goal is to create a very simple pipeline (similar to the example from the lectures).

##Jenkins Installation and Configuration

Using the following URL:'https://www.jenkins.io/doc/book/installing/', I managed to download a jenkins.war file and consequently executed it by opening a bash window on the file location and executing command:

```
java -jar jenkins.war
```

After executing the command, I opened a new browser with URL:
```
localhost:8080
```
and proceded with the installation and configuration of jenkins by installing the suggested plugins, and selecting a username and password.

## Pipeline Script

In order to execute a Pipeline I configured a script, so first I selected New Item on the main menu, choosing a name for the job. After this, a configured the Pipeline by choosing Pipeline script from SCM, selecting the GIT option.

After this, I selected my Bitbucket credentials, that were already configured on the credentials page, located in the main menu as well as the correct path to the Jenkinsfile, located on 'ca2/part1'.

Then, Jenkins was able to checkout from my Bitbucket repository the script that was supposed to, and execute it:

```
pipeline {
             agent any

         	stages {
         	    stage('Checkout') {
         	        steps {
         	            echo 'Checking out...'
         	            git credentialsId: 'diogoamr-bitbucket', url:'https://1191756@bitbucket.org/1191756/devops-19-20-b'
         	        }
         	    }
         	    stage('Assemble') {
         	        steps {
         	            echo 'Assembling...'
         	            bat 'cd ca2/Part1 & gradlew clean assemble'
         	        }
         	    }
         	    stage('Test') {
         	        steps {
         	            echo 'Testing...'
         	            bat 'cd ca2/Part1 & gradlew test'
         	            junit 'ca2/Part1/build/test-results/test/*.xml'

         	        }
         	    }
         	    stage('Archiving') {
         	        steps {
         	            echo 'Archiving...'
         	            archiveArtifacts 'ca2/Part1/build/distributions/*'
         	        }
         	    }
         	}
         }
```

All stages went smoothly and the Test Results showed that all tests taken passed.