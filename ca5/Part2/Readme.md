# Class Assignment 5 Part2

## Objective

The goal of the Part 2 of this assignment is to create a pipeline in Jenkins to build the tutorial spring boot application, gradle "basic" version (developed in CA2, Part2).

I started this assignment with the conviction that I could resolve it the conventional way, so I downloaded the jenkins.war file and executed the following command, in order to execute Jenkins:

```jenkins

java -jar jenkins.war                                                       

```

### Jenkinsfile

In order to run Jenkins Pipeline using a remote repository I uploaded that said file to my repository, using GIT from my local host to my Bitbucket account.

I wrote the following script pipeline:

```
pipeline {
             agent any

         	stages {
         	    stage('Checkout') {
         	        steps {
         	            echo 'Checking out...'
         	            git credentialsId: 'diogoamr-bitbucket', url:'https://1191756@bitbucket.org/1191756/devops-19-20-b'
         	        }
         	    }

```

This stage will do a checkout from my bitbucket repository, using git credentialsID, already saved in Jenkins credentials page.

```
stage('Assemble') {
         	        steps {
         	            echo 'Assembling...'
         	            bat 'cd ca2/Part2 & gradlew clean assemble'
         	        }
         	    }
         	    stage('Test') {
         	        steps {
         	            echo 'Testing...'
         	            bat 'cd ca2/Part2 & gradlew test'
         	            junit 'ca2/Part2/build/test-results/test/*.xml'

         	        }
         	    }
         	    stage('Javadoc') {
                    steps {
                        echo 'Generating Javadoc...'
                        bat 'cd ca2/Part2 & gradlew javadoc'
                        publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'CA2/part2/build/docs/javadoc', reportFiles: 'index.html', reportName: 'HTML Report', reportTitles: ''])
                    }
                }
         	    stage('Archiving') {
         	        steps {
         	            echo 'Archiving...'
         	            archiveArtifacts 'ca2/Part2/build/libs/*'
         	        }
         	    }
```
These four stages will, respectivelly, assemble a gradle project, run tests using junit, generate Javadoc using HTML Publisher Plugin that was specifically installed and archive the respective artifacts.

```
stage('Docker Image') {
                    steps {
                        echo 'Publishing docker image...'
                        script{
                            docker.build("ca5part2:${env.BUILD_ID}", "ca2/Part2").push()
                        }

```

This final stage didn´t go as planned because Jenkins needs to execute Docker on a host system and I couldn't install Docker Desktop or Docker Toolbox due to a hyper-v issue that doesn't recognize that hyper-v is enabled or disabled.

Consequently, I couldn't execute this final stage, reason why I decided to run the project on a Virtual Machine, from where I could execute Docker Engine and Jenkins from.

## Installing Jenkins on a Virtual Machine

In order to install Jenkins, after installing Java8, I executed the following commands:

```
 wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -
```
Then added the following entry in /etc/apt/sources.list:
```
  deb https://pkg.jenkins.io/debian binary/

```
Updated the local package index, then finally installed Jenkins:
```
  sudo apt-get update
  sudo apt-get install jenkins
```
## Executing Jenkins Script Pipeline

After changing to the VM Ubuntu and executing the necessary modifications on Jenkinsfile, mainly syntax alterations, I had multiple issues arising mainly from file permissions.

One of the most difficult arose from gradle wrapper permission, gradlew couldn't run because there was no execute permission.

I managed to resolve it, by giving execussion permissions and committing the alterations to the repository, executing the following commands:
```
git update-index --chmod=+x gradlew
git add .
git commit -m "Changing permission of gradlew"
git push
```
After this issue all stages went smoothly until the last one, building a docker image.

This time, while I was in the process of dealing with these issues, a major error came up, my VM was full and Jenkins wasn't able to archive any artifacts.

I looked for various solutions, namely resizing my VM vdi drive, increasing its size, or to simply delete other artifacts or unused images.

Given so, at the time I decided to try to resize my disk, and although I was able to increase disk memory, I wasn't able to successfully refit it,  because I couldn't install a program called GParted that is able to refit the partitions of the vdi disk.

I decided to create a new VM with 30 gb of vdi disk memory.

###Problems with Jenkins re-installation

A new and better VM was created and the required dependencies and different technologies were installed, however I had major problems installing Jenkins this time around.

Somehow I couldn't run the update, an error kept emerging:

```
The repository is not signed.
N: Updating from such a repository can't be done securely, and is therefore disabled by default.
N: See apt-secure(8) manpage for repository creation and user configuration details.
```
I managed to deal with it by executing this command:

```
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 5FCBF54A
```
This command adds a public key that Jenkins wasn't able to add due to system security specifications, this command adds it manually.

Back to Jenkins Pipeline Script, I re-started to execute the build however got stuck again this time with a permission situation:
```
+ ./ gradlew javadoc
/var/lib/jenkins/workspace/teste1@tmp/durable-0fad3509/script.sh: 1: /var/lib/jenkins/workspace/teste1@tmp/durable-0fad3509/script.sh: ./: Permission denied
```
This time was easy to deal with, there was a blank space between "./" and "gradlew javadoc", but due to the designation "Permission denied" I thought that it was a permission problem and lost too much time looking at it.

All stages went smoothly except the last one, building a Docker Image.

I faced different errors, one of which was a little harder to get by, another permission denied error:
```
+ docker build --tag web_c5:23 ca2/Part2
Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Post http://%2Fvar%2Frun%2Fdocker.sock/v1.40/build?buildargs=%7B%7D&... dial unix /var/run/docker.sock: connect: permission denied
```
I resolved this issue by executing the following command:
```
sudo chmod 777 /var/run/docker.sock
```
I also had some issues with the script, mainly because I wasn´t assigning the correct path, but finally came up with a solution that properlly executed the Pipeline script:
```
sh 'cd ca2/Part2'
sh 'docker build --tag web_c5:23 ca2/Part2'
```
The script executed correctly however I wasn't able to publish the image on Docker Hub because I could only execute the commands to publish by using sudo on the VM and I didn´t have time to search for a solution to circle this issue.

I opted to publish the image directly from my VM, using the following commands:
```
sudo docker login
username:
password:
sudo docker tag web_ca5:23 1191756/web_ca5:23
sudo docker push 1191756/web_c5
```

Unfortunatelly I couldn't finish the upload because the size of the image was of 1.5 gb. 

A possible solution for the sudo issue would be to configure Docker daemon as a non-root user if the prerequisites were complied.

Rootless mode allows running the Docker daemon and containers as a non-root user, for the sake of mitigating potential vulnerabilities in the daemon and the container runtime.

The installation script is available at https://get.docker.com/rootless.

## Alternative to Jenkins - Analysis

I analised different technologies that could replace jenkins and my interest has gone to one in particular, Buddy.

This happened specially because I had a lot of problems on the deployment of the Jenkins Pipeline mainly because of particular circunstances related to Ubuntu and Virtualization configurations.

Well, Buddy is a SaaS offering, meaning there is nothing to get running on the client's side!

Instalation? Not needed.

Maintenance? Not needed.

Jenkins servers have been known to crash and fail regularly and have required tuning in the past, as I also experienced, although probably because of a bad configuration.

Buddy is a managed offering, meaning you don't need to actively maintain it. This is covered as part of your subscription.

Buddy comes prepackaged with a plethora of integrations that have been developed in house. As such, plugins don't interfere with one another and quality can be assured centrally.

In terms of Pipelines, both are very similar, both have the option to directly configure the Pipeline and offer a description of each stage.

It is a given that a SaaS solution poses far less setup complexity than a locally hosted  alternative.
